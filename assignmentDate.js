let assignmentDate = '1/21/2019';
let daysToAdd = 7;

// convert to Date instance
let currentDate = new Date(assignmentDate)

// add 7 days to get new due date
let dueDate = new Date(currentDate.setDate(currentDate.getDate() + daysToAdd))
console.log(dueDate);

// month starts at 0 so add one
let month = dueDate.getMonth() + 1;
let day = dueDate.getDate();
let year = dueDate.getFullYear();

// if month is single digit, add a 0 in front
if (month < 10) {
	month = '0' + month;
} else {
	month.toString();
}

// if day is a single digit, add a 0 in front
if (day < 10) {
	day = '0' + day;
} else {
	day.toString();
}

// 
let dueDateString = `${year}${'-'}${month}${'-'}${day}`;
console.log(dueDateString);